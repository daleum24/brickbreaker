(function(root) {
  var Game = root.Game = (root.Game || {});

  var Paddle = Game.Paddle = function(maxX, maxY){
    this.COLOR  = "black";
    this.length = 80
    this.width  = 10
    this.xPos   = 210 // (maxX/2) - (this.length/2);
    this.yPos   = 490 // maxY - 10;
    this.xVel   = 0;
    this.yVel   = 0;
  }

  Paddle.prototype.move = function(maxX, maxY){
    if ( (this.xPos + this.length) > maxX ){
      this.xPos = ( maxX - this.length );
    } else if (this.xPos < 0){
      this.xPos = 0;
    }
  }

  Paddle.prototype.draw = function(ctx){
    ctx.beginPath();
    ctx.strokeStyle = this.color;
    ctx.fillRect(this.xPos, this.yPos, this.length, this.width)
    ctx.stroke();
  }

  Paddle.prototype.impulse = function(increment){
    this.xPos += increment;
  }
})(this);









