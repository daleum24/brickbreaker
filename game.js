(function(root) {
  var Game = root.Game = (root.Game || {});

  var Screen = Game.Screen = function(ctx, dimX, dimY) {
    this.dimX = dimX;
    this.dimY = dimY;
    this.ctx  = ctx;
    this.paddle = new Game.Paddle(dimX,dimY);
    this.ball   = new Game.Ball([10,5]);
    this.bricks = [];
    for (var i = 0; i < 12; i++) {
      for (var j = 0; j < 1; j++) {
        this.bricks.push(new Game.Brick(((i*40) + 5 ),((j*10) + 5)))
      }
    }
    alert(this.bricks);
  };

  Screen.prototype.move = function(){
    this.paddle.move(this.dimX, this.dimY);
    this.ball.move(this.dimX, this.dimY);
    this.isBounce();
  };

  Screen.prototype.draw = function(){
    var self = this;
    this.ctx.fillStyle = "black";
    this.ctx.fillRect(0,0,this.dimX,this.dimY);
    this.ctx.clearRect(0,0,this.dimX,this.dimY)
    this.paddle.draw(this.ctx);
    this.ball.draw(this.ctx);
    this.bricks.forEach( function(brick) {
      brick.draw(self.ctx)
    });
  };

  Screen.prototype.step = function(){
    this.move();
    this.hitBrick(this.bricks);
    this.draw();
  };

  Screen.prototype.start = function(){
    this.bindKeyHandlers();
    this.timer = setInterval(this.step.bind(this), 30);
  };

  Screen.prototype.stop = function() {
    clearInterval(this.timer);
    alert("GAME OVER!");
  };

  Screen.prototype.isBounce = function(){
    var xPaddle = _.range(this.paddle.xPos, this.paddle.xPos + this.paddle.length);
    var yPaddle = this.paddle.yPos;
    if ((this.ball.yPos + this.ball.radius > yPaddle) && (_.contains(xPaddle, this.ball.xPos))){
      this.ball.yVel = -(this.ball.yVel);
    };
  };

  Screen.prototype.bindKeyHandlers = function() {
    var self = this;
    key('left' , function(){ self.paddle.impulse(-30) });
    key('right', function(){ self.paddle.impulse( 30) });
  };

  Screen.prototype.hitBrick = function(brick_arr){
    var self = this;
    brick_arr.forEach(function(brick){
      var xBrick = _.range(brick.xPos, brick.xPos + brick.length);
      var yBrick = brick.yPos + brick.width;
      if ((self.ball.yPos - self.ball.radius < yBrick) && (_.contains(xBrick, self.ball.xPos))){
        self.ball.yVel = -(self.ball.yVel);
        brick.xPos = 500;
      }
    });
  };

})(this);