(function(root) {
  var Game = root.Game = (root.Game || {});

  var Ball = Game.Ball = function(speed){
    this.COLOR  = "red";
    this.radius = 5
    this.xPos   = 250 // (maxX/2) - (this.length/2);
    this.yPos   = 250 // maxY - 10;
    this.xVel   = speed[0];
    this.yVel   = speed[1];
  }

  Ball.prototype.move = function(maxX, maxY){
    this.xPos += this.xVel;
    this.yPos += this.yVel;

    if (this.xPos < 0){
      this.xPos = 1;
      this.xVel = -(this.xVel);
    } else if (this.xPos > maxX) {
      this.xPos = maxX;
      this.xVel = -(this.xVel);
    };
    if (this.yPos < 0){
      this.yPos = 1;
      this.yVel = -(this.yVel);
    };
  }

  Ball.prototype.draw = function(ctx){
    ctx.beginPath();
    ctx.strokeStyle = this.color;
    ctx.arc(this.xPos, this.yPos, this.radius, 0, Math.PI*2)
    ctx.stroke();
  };


})(this);
