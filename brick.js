(function(root) {
  var Game = root.Game = (root.Game || {});

  var Brick = Game.Brick = function(xPos, yPos){
    this.COLOR  = "red";
    this.length = 35
    this.width  = 10
    this.xPos   = xPos
    this.yPos   = yPos
  }

  Brick.prototype.draw = function(ctx){
    ctx.beginPath();
    ctx.strokeStyle = this.color;
    ctx.fillRect(this.xPos, this.yPos, this.length, this.width)
    ctx.stroke();
  }

})(this);

